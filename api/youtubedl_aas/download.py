import os
import logging
from datetime import datetime, timedelta
from youtube_dl import YoutubeDL
from youtubedl_aas.models import Progress, Video
from youtubedl_aas.storage.blob_storage import store_delete_file
from youtubedl_aas.storage.redis_storage import set_progress
from youtubedl_aas.gif_processing import convert_to_gif, optimize_gif
from youtubedl_aas.async_wrap import async_wrap
logger = logging.getLogger(__name__)

async def download(video: Video, id, filename):
    progress = Progress(id=id)

    def update_progress():
        """ Update Redis with new progress """

        if progress.last_update < datetime.utcnow() + timedelta(seconds=2):
            progress.last_update = datetime.utcnow()
            set_progress(progress)

    def progress_hook(data):
        """ Update youtube-dl progress hook """

        if data["status"] == "error":
            # save error status
            logger.error("Progress status is in error")
        progress.status = data["status"]
        update_progress()

    def convert_progress_hook(current: int, total: int):
        """ Update status with the converting progress """
        progress.status = "converting"
        progress.current = current
        progress.total = total
        update_progress()

    ydl_opts = {
        "progress_hooks": [progress_hook],
        "outtmpl": filename,
        "writethumbnail": True
    }
    with YoutubeDL(ydl_opts) as ydl:
        # save downloading status
        update_progress()
        download_task = async_wrap(ydl.extract_info) #extract info also downloads
        info = await download_task(video.url)
        progress.title = info["title"]
        progress.source = info["extractor"]
        if "thumbnails" in info:
            thumbnails = info["thumbnails"]
            thumbnail = next((x for x in thumbnails if "filename" in x), None)
            if thumbnail:
                progress.thumbnail_url = store_delete_file(thumbnail["filename"])

        store_location = os.path.splitext(filename)[0]
        progress.gif_filename = store_location + ".gif"
        
        if video.convert2Gif:
            if os.path.exists(progress.gif_filename) == False:
                progress.status = "converting"
                # start converting to gif
                await async_wrap(convert_to_gif(progress.id, filename, progress.gif_filename, convert_progress_hook))
                if video.optimize:
                    await async_wrap(optimize_gif(progress.gif_filename))
            progress.gif_url = store_delete_file(progress.gif_filename)
        progress.mp4_url = store_delete_file(filename)
        progress.status = "finished"
        update_progress()
