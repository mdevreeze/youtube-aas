const state = {
  videos: []
};

const getters = {
  allVideos: (state) => state.videos
};

const actions = {
  async getNewVideo({ commit }, id) {
    const response = await fetch(`${process.env.VUE_APP_API_BASE_URL}/${id}`)
    commit("newVideo", await response.json());
  },
  async submitUrl({ dispatch }, { url, convert2Gif, optimize}) {
    await fetch(process.env.VUE_APP_API_BASE_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        url,
        convert2Gif,
        optimize
      })
    })
    .then(r => r.json())
    .then(r => { dispatch("checkProgress", r.id)})
  },
};

const mutations = {
  newVideo: (state, video) => {
    console.log(video)
    state.videos.unshift(video);
    if (state.videos.length > 8) {
      state.videos.pop();
    }
  }
};


export default {
  state,
  getters,
  actions,
  mutations
}
