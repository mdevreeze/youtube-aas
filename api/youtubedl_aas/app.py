from __future__ import unicode_literals
import os
import logging
import random
import time
import string
from fastapi import FastAPI, HTTPException, BackgroundTasks, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from youtube_dl import YoutubeDL, DownloadError
from youtubedl_aas.config import ALLOW_ORIGINS
from youtubedl_aas.models import Progress, Video
from youtubedl_aas.storage.redis_storage import get_status, set_status
from youtubedl_aas.download import download

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.warning("App started")

dir_path = os.path.dirname(os.path.realpath(__file__))
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=[ALLOW_ORIGINS],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

output_path = dir_path + "/output/"
filename_format_string = output_path + "%(id)s-%(extractor)s.%(ext)s"

# @app.middleware("http")
# async def log_requests(request: Request, call_next):
#     idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
#     properties_start = {"rid": idem, "path": request.url, "method": request.method}
#     logger.info("Request started", extra=properties_start)
#     start_time = time.time()

#     response = await call_next(request)

#     process_time = (time.time() - start_time) * 1000
#     formatted_process_time = "{0:.2f}".format(process_time)
#     properties_end = {
#         "rid": idem,
#         "completed_in": formatted_process_time,
#         "status_code": response.status_code,
#     }
#     logger.info("Request completed", extra=properties_end)

#     return response


@app.get("/{id}/status")
def retrieve_status(id: str):
    """Get video download/conversion progress"""
    progress = get_status(id)
    progress_number = 0
    if progress.status == "finished":
        progress_number = 100
    elif progress.total and progress.current:
        progress_number = (progress.current / progress.total) * 100
    return {
        "status": progress.status,
        "progress": progress_number,
    }


@app.get("/{id}")
def get_by_id(id: str):
    progress = get_status(id)
    return {
        "id": id,
        "title": progress.title,
        "source": progress.source,
        "gif_url": progress.gif_url,
        "mp4_url": progress.mp4_url,
        "thumbnail_url": progress.thumbnail_url
    }

@app.get("/")
async def get(request: Request):
    return {"Hello": "World"}


@app.post("/")
async def post(video: Video, background_tasks: BackgroundTasks):
    """Download video endpoint"""
    
    id=""
    try:
        with YoutubeDL({"outtmpl": filename_format_string}) as ydl:
            info = ydl.extract_info(video.url, download=False)
            id='{extractor}-{id}'.format(**info)
            filename = ydl.prepare_filename(info)
            background_tasks.add_task(download, video, id, filename)
        return JSONResponse(status_code=202, content={"status": "downloading", "id": id, "title": info["title"], "source": info["extractor"]})
    except DownloadError as download_err:
        set_status(id, "error") 
        logger.exception(download_err)
        raise HTTPException(
            status_code=500,
            detail="Youtube-dl download error",
            headers={"X-Error": str(download_err)},
        )
    except Exception as exception:
        set_status(id, "error")
        logger.exception(exception)
        raise HTTPException(
            status_code=500,
            detail="Unknown error",
            headers={"X-Error": str(exception)},
        )
