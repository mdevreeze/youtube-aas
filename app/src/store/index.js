import Vuex from "vuex";
import Vue from "vue";
import videos from "./modules/videos";
import progress from "./modules/progress"

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    videos,
    progress
  }
});
