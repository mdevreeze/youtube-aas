const state = {
  progress: 0,
  status: "",
  timeout: undefined
};

const getters = {
  getProgress: (state) => state.progress,
  getStatus: (state) => state.status
};

const actions = {
  async checkProgress({ commit, dispatch, state }, id) {
    if (!state.timeout) {
      const timeout = new Date();
      timeout.setSeconds(timeout.getSeconds() + 120)
      commit("setTimeout", timeout)
    }
    if (state.status === "finished" || state.status === "error" || Date.now() > state.timeout) {
      return
    }

    fetch(`${process.env.VUE_APP_API_BASE_URL}/${id}/status`, {
      headers: { 'Accept': 'application/json' }
    })
      .then(r => r.json())
      .then(r => {
        commit("setProgress", r.progress)
        commit("setStatus", r.status)
        if (r.status === "finished") {
          dispatch("getNewVideo", id)
        } else {
          setTimeout(() => dispatch("checkProgress", id), 1000);
        }
      })

  },
  resetStatus({ commit }) {
    commit("resetStatus")
  }
};

const mutations = {
  setProgress: (state, progress) => state.progress = progress,
  setStatus: (state, status) => state.status = status,
  setTimeoutId: (state, timeoutId) => state.timeoutId = timeoutId,
  resetStatus: (state) => {
    state.process = 0
    state.status = ""
    state.timeoutId = undefined
  }
};


export default {
  state,
  getters,
  actions,
  mutations
}
