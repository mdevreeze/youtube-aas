import redis
from youtubedl_aas.models import Progress
from youtubedl_aas.config import REDIS_KEY


def get_redis_connection():
    r = redis.Redis(
        host="ydlaas.redis.cache.windows.net",
        port=6380,
        db=0,
        password=REDIS_KEY,
        ssl=True,
    )
    return r


def get_status(identifier):
    con = get_redis_connection()
    status = Progress(id=identifier, status="Unknown")
    status = Progress.parse_raw(con.get("status:" + identifier))
    return status


def set_progress(progress: Progress):
    con = get_redis_connection()
    json_str = progress.json()
    con.set("status:" + str(progress.id), json_str, ex=36000)


def set_status(id, status):
    progress = get_status(id)
    progress.status = status
    set_progress(progress)
